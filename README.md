### dbot - revived

The motivation for this fork came from trying to make the trusty bot of our IRC network work with new infrastructure, until all features made their way into its successor.

Note that this project is not production worthy - it contains a lot of hardcoded debug lines and untidy code. Whether it will see prettification anytime soon, I cannot tell.

So far, the following issues were tackled successfully:

- NickServ identification
- NickServ user retrieval
- Quote shortcuts (~foo)
- Channel statistics (cstats)

 This makes the bot usable on an IRC network powered by the Ergo IRCd, which is dbot was not happy communicate with at all when I first tried to run it.

The following issues are known, but are assigned low priority:

- No PASS/SASL authentication - it is hackable with a PASS line right after the USER command, however that prevents some parts of the bot from initializing. To use NickServ authentication, allowing the bot to start properly, the strict username checking feature in Ergo needs to be disabled
- Lots of ENOENT errors on startup
- Channel mode detection and assignment
- Some sites in the webinterface don't load / show errors

The following issues are known, and await investigation:

- Upon issuing some administrative commands, apparently a sort of "queue flush" gets trigered, causing dozens of unban events to make the bot unresponsive for a while

The following issues are resolved, but are not available in this repository, due to files not being accessible:

- WHO parsing for user management commands
- nban/nunban, with newly added ipban

The directory modules-stock/ is the stock modules directory, kept for safekeeping.
The directory modules/ contains changed modules.

The jsbot/ directory is not populated in the stock repository, however its contents are included here, as a few essential changes have been implemented there as well.

Tested with node v10.

Note: TripSit IRC related modules are to be found in a seperate repository.

---

### Original README.md

# DBot IRC Bot

## Introduction

DBot is an IRC bot which aims to be the fanciest IRC bot around - On
the general standard of software fanciness, dbot is statistically rated as being 
'82% the same as bathing in fine, fine grape juice.'

Please note that this documentation is not complete and is a work in progress, 
given I started it rather a long time after I began development of the project. 
Please don't judge me too harshly for this as I am, in fact, mildly allergic to
writing documentation.

## Getting Started

To get started with DBot, you first need to decide on a database system to use.
DBot uses the [databank](http://github.com/e14n/databank) library, and each
module can be configured to use any database driver databank supports in its
respective config.json file. There is currently no default database driver
option.

The default for all modules is the 'redis' driver, and you can simply install
the Redis server to get going.

Once you have that set up, you can install DBot's dependencies, configure and 
run the bot for the first time with the following command:

```
./install
```

## Upgrading

If you have used a previous version of DBot, then you can migrate most data
using the [dbot-migrate](https://github.com/reality/dbot-migrate) module.
Instructions on how to run this are included in the repository - remember to
remove db.json after migration, otherwise the instance will be slow!
